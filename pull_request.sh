#!/bin/sh

set -euo pipefail


commit() {
    # get the files size more than 50MB but not in .git folder
    local large_files=$(find . -path '*/.git' -prune -o -type f -size +50M -print)

    if [ ! -z "${large_files}" ]; then
        git lfs install
        git lfs track --filename ${large_files}
    fi
    git add .

    git commit -m 'apply new package ci pull request'

}


commit
